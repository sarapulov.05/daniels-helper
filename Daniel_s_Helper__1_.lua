script_version(2)
script_name('Daniels Helper')

require 'lib.moonloader'
local imgui = require 'imgui'
local key = require 'vkeys'
local lib = require 'lib.samp.events'
local encoding = require 'encoding'
encoding.default = 'CP1251'
u8 = encoding.UTF8

local playerTargeting = -1

local toggle = false
local ImBool1 = imgui.ImBool(false)
local ImBool2 = imgui.ImBool(false)
local ImBool3 = imgui.ImBool(false)
local wind = imgui.ImBool(false)
local settings_window = imgui.ImBool(false)

function imgui.OnDrawFrame()
  	imgui.ShowCursor = wind.v
		local sw, sh = getScreenResolution()
    if wind.v then
      imgui.SetNextWindowPos(imgui.ImVec2(sw / 2, sh / 2), imgui.Cond.FirstUseEver, imgui.ImVec2(0.5, 0.5)) --��������� ��������� ����
      imgui.SetNextWindowSize(imgui.ImVec2(600, 470), imgui.Cond.FirstUseEver)--������ ��������� ����
      imgui.Begin('DanielsHelper', wind)
        imgui.PushItemWidth(150)
        imgui.SetCursorPos(imgui.ImVec2(435, 25))
    end imgui.PopItemWidth()
      imgui.SetCursorPos(imgui.ImVec2(7, 50))
      imgui.BeginChild('##id', imgui.ImVec2(580, 30), true)
        if (playerTargeting >= 0) then
          local name = sampGetPlayerNickname(tostring(playerTargeting))
          imgui.Text(u8' ����: '..name..'('..tostring(playerTargeting)..')')
        end
        if (playerTargeting < 0) then
          imgui.Text(u8' ����: Not Found')
        end
      imgui.EndChild()
       imgui.SetCursorPos (imgui.ImVec2(7, 85))
       imgui.BeginChild('##1', imgui.ImVec2(190, 180), true)
        local btn_size = imgui.ImVec2(-0.1, 0)
        imgui.Text(u8'�������')
        imgui.Separator()
          if imgui.Button(u8'����������� � ���� �����', btn_size) then
              sampSendChat("/fam ������!")
          end
          if imgui.Button(u8'����������� ������ �����!', btn_size) then
              sampSendChat('/fam ����� ���������� � ��� ����!')
          end
          if imgui.Button(u8'���������� � ��', btn_size) then
             sampSendChat('/fam ���������� � ��! �������, ��������!')
          end
       imgui.EndChild()
       imgui.BeginChild('##2', imgui.ImVec2(190, 180), true)
       imgui.Text(u8'����')
       imgui.Separator()
          if imgui.Button(u8'�����', btn_size) then
              sampSendChat("/armour")
          end
          if imgui.Button(u8'�����', btn_size) then
              sampSendChat('/mask')
          end
          if imgui.Button(u8'�������', btn_size) then
              sampSendChat('/phone')
          end
          if imgui.Button(u8'����������', btn_size) then
              sampSendChat('/stats')
          end
          if imgui.Button(u8'����� �� ����', btn_size) then
              sampProcessChatInput ("/q")
          end
        imgui.EndChild()
------------------------------

        imgui.SameLine()

--2 ����
------------------------------
        imgui.SetCursorPos(imgui.ImVec2(204, 85))
        imgui.BeginChild('##3', imgui.ImVec2(190, 180), true)
        imgui.Text(u8'��������')
        imgui.Separator()
          if imgui.Button(u8'�������� �� 300$', btn_size) then
             sampSendClickTextdraw(669)
          end
          if imgui.Button(u8'����� 3�', btn_size) then
             sampSendChat('/usedrugs 3')
          end
          if imgui.Button(u8'������ ������ �� 500$', btn_size) then
             sampSendClickTextdraw(667)
          end
        imgui.EndChild()
        imgui.SetCursorPos(imgui.ImVec2(204, 269))
        imgui.BeginChild('##4', imgui.ImVec2(190, 180), true)
        imgui.Text('Cars')
        imgui.Separator()
          if imgui.Button(u8'���������', btn_size) then
              sampSendChat("/fillcar")
          end
          if imgui.Button(u8'�������', btn_size) then
              sampSendChat('/repcar')
          end
          if imgui.Button(u8'������� ���', btn_size) then
              sampSendChat('/lock')
          end
          if imgui.Button(u8'��������/�������� �����', btn_size) then
              sampSendChat('/key')
          end
        imgui.EndChild()
------------------------------------

        imgui.SameLine()
--3 ����
------------------------------------
        imgui.SetCursorPos(imgui.ImVec2(401, 85))
        imgui.BeginChild('##5', imgui.ImVec2(190, 180), true)
        imgui.Text(u8'�������������� � ��������')
        imgui.Separator()
          if imgui.Button(u8'�����', btn_size) then
            sampSendChat('/trade '..tostring(playerTargeting))
          end
          if imgui.Button(u8'�������', btn_size) then
            sampSendChat('/showpass '..tostring(playerTargeting))
          end
          if imgui.Button(u8'���. �����', btn_size) then
            sampSendChat('/showmc '..tostring(playerTargeting))
          end
          if imgui.Button(u8'��������', btn_size) then
            sampSendChat('/showlic '..tostring(playerTargeting))
          end
          if imgui.Button(u8'�������� 10�', btn_size) then
            sampSendChat('/pay '..tostring(playerTargeting)..' 10000')
          end
        imgui.EndChild()
        imgui.SetCursorPos(imgui.ImVec2(401, 269))
        imgui.BeginChild('##6', imgui.ImVec2(190, 180), true)
        imgui.Text(u8'� ����������')
        imgui.Separator()
        imgui.EndChild()
      imgui.End()
end

function main()
	if not isSampfuncsLoaded() or not isSampLoaded() then
          return
    end
	autoupdate("https://api.jsonbin.io/b/5e0a2a8102ce5777b8b4d30c", '['..string.upper(thisScript().name)..']: ', "https://gist.githubusercontent.com/DrLesbow/61dbf47b8a0c778e28fd3bc6110b2d74/raw/039eb1230e87c057d8a54967c41497b904b1073a/Daniels%2520Helper")
    while not isSampAvailable() do
        wait(100)
    end
    sampRegisterChatCommand('danh', function() wind.v = not wind.v end)
    sampAddChatMessage('{FF0000}[Daniels Helper]: {FFFFFF}������� ��������', -1)
    sampAddChatMessage('{FF0000}[Daniels Helper]: {FFFFFF}��������� - Q+E ��� /danh', -1)
  while true do
    wait(0)
    if wasKeyPressed(key.VK_E) and wasKeyPressed(key.VK_Q) then -- ���������
        wind.v = not wind.v
    end
    local valid, ped = getCharPlayerIsTargeting(PLAYER_HANDLE)
     if valid and doesCharExist(ped) then
     local result, id = sampGetPlayerIdByCharHandle(ped)
     if result then
       playerTargeting = id
     end
   end
    imgui.Process = wind.v
  end
end


function apply_custom_style()
    imgui.SwitchContext()
    local style = imgui.GetStyle()
    local colors = style.Colors
    local clr = imgui.Col
    local ImVec4 = imgui.ImVec4

    style.WindowRounding = 2.0
    style.WindowTitleAlign = imgui.ImVec2(0.5, 0.84)
    style.ChildWindowRounding = 2.0
    style.FrameRounding = 2.0
    style.ItemSpacing = imgui.ImVec2(5.0, 4.0)
    style.ScrollbarSize = 13.0
    style.ScrollbarRounding = 0
    style.GrabMinSize = 8.0
    style.GrabRounding = 1.0

    colors[clr.FrameBg]                = ImVec4(0.48, 0.16, 0.16, 0.54)
    colors[clr.FrameBgHovered]         = ImVec4(0.98, 0.26, 0.26, 0.40)
    colors[clr.FrameBgActive]          = ImVec4(0.98, 0.26, 0.26, 0.67)
    colors[clr.TitleBg]                = ImVec4(0.04, 0.04, 0.04, 1.00)
    colors[clr.TitleBgActive]          = ImVec4(0.48, 0.16, 0.16, 1.00)
    colors[clr.TitleBgCollapsed]       = ImVec4(0.00, 0.00, 0.00, 0.51)
    colors[clr.CheckMark]              = ImVec4(0.98, 0.26, 0.26, 1.00)
    colors[clr.SliderGrab]             = ImVec4(0.88, 0.26, 0.24, 1.00)
    colors[clr.SliderGrabActive]       = ImVec4(0.98, 0.26, 0.26, 1.00)
    colors[clr.Button]                 = ImVec4(0.98, 0.26, 0.26, 0.40)
    colors[clr.ButtonHovered]          = ImVec4(0.98, 0.26, 0.26, 1.00)
    colors[clr.ButtonActive]           = ImVec4(0.98, 0.06, 0.06, 1.00)
    colors[clr.Header]                 = ImVec4(0.98, 0.26, 0.26, 0.31)
    colors[clr.HeaderHovered]          = ImVec4(0.98, 0.26, 0.26, 0.80)
    colors[clr.HeaderActive]           = ImVec4(0.98, 0.26, 0.26, 1.00)
    colors[clr.Separator]              = colors[clr.Border]
    colors[clr.SeparatorHovered]       = ImVec4(0.75, 0.10, 0.10, 0.78)
    colors[clr.SeparatorActive]        = ImVec4(0.75, 0.10, 0.10, 1.00)
    colors[clr.ResizeGrip]             = ImVec4(0.98, 0.26, 0.26, 0.25)
    colors[clr.ResizeGripHovered]      = ImVec4(0.98, 0.26, 0.26, 0.67)
    colors[clr.ResizeGripActive]       = ImVec4(0.98, 0.26, 0.26, 0.95)
    colors[clr.TextSelectedBg]         = ImVec4(0.98, 0.26, 0.26, 0.35)
    colors[clr.Text]                   = ImVec4(1.00, 1.00, 1.00, 1.00)
    colors[clr.TextDisabled]           = ImVec4(0.50, 0.50, 0.50, 1.00)
    colors[clr.WindowBg]               = ImVec4(0.06, 0.06, 0.06, 0.94)
    colors[clr.ChildWindowBg]          = ImVec4(1.00, 1.00, 1.00, 0.00)
    colors[clr.PopupBg]                = ImVec4(0.08, 0.08, 0.08, 0.94)
    colors[clr.ComboBg]                = colors[clr.PopupBg]
    colors[clr.Border]                 = ImVec4(0.43, 0.43, 0.50, 0.50)
    colors[clr.BorderShadow]           = ImVec4(0.00, 0.00, 0.00, 0.00)
    colors[clr.MenuBarBg]              = ImVec4(0.14, 0.14, 0.14, 1.00)
    colors[clr.ScrollbarBg]            = ImVec4(0.02, 0.02, 0.02, 0.53)
    colors[clr.ScrollbarGrab]          = ImVec4(0.31, 0.31, 0.31, 1.00)
    colors[clr.ScrollbarGrabHovered]   = ImVec4(0.41, 0.41, 0.41, 1.00)
    colors[clr.ScrollbarGrabActive]    = ImVec4(0.51, 0.51, 0.51, 1.00)
    colors[clr.CloseButton]            = ImVec4(0.41, 0.41, 0.41, 0.50)
    colors[clr.CloseButtonHovered]     = ImVec4(0.98, 0.39, 0.36, 1.00)
    colors[clr.CloseButtonActive]      = ImVec4(0.98, 0.39, 0.36, 1.00)
    colors[clr.PlotLines]              = ImVec4(0.61, 0.61, 0.61, 1.00)
    colors[clr.PlotLinesHovered]       = ImVec4(1.00, 0.43, 0.35, 1.00)
    colors[clr.PlotHistogram]          = ImVec4(0.90, 0.70, 0.00, 1.00)
    colors[clr.PlotHistogramHovered]   = ImVec4(1.00, 0.60, 0.00, 1.00)
    colors[clr.ModalWindowDarkening]   = ImVec4(0.80, 0.80, 0.80, 0.35)
end
apply_custom_style()

function imgui.TextQuestion(text)
	imgui.TextDisabled('(?)')
	if imgui.IsItemHovered() then
		imgui.BeginTooltip()
		imgui.PushTextWrapPos(450)
		imgui.TextUnformatted(text)
		imgui.PopTextWrapPos()
		imgui.EndTooltip()
	end
end
function autoupdate(json_url, prefix, url)
  local dlstatus = require('moonloader').download_status
  local json = getWorkingDirectory() .. '\\'..thisScript().name..'-version.json'
  if doesFileExist(json) then os.remove(json) end
  downloadUrlToFile(json_url, json,
    function(id, status, p1, p2)
      if status == dlstatus.STATUSEX_ENDDOWNLOAD then
        if doesFileExist(json) then
          local f = io.open(json, 'r')
          if f then
            local info = decodeJson(f:read('*a'))
            updatelink = info.updateurl
            updateversion = info.latest
            f:close()
            os.remove(json)
            if updateversion ~= thisScript().version then
              lua_thread.create(function(prefix)
                local dlstatus = require('moonloader').download_status
                local color = -1
                sampAddChatMessage((prefix..'���������� ����������. ������� ���������� c '..thisScript().version..' �� '..updateversion), color)
                wait(250)
                downloadUrlToFile(updatelink, thisScript().path,
                  function(id3, status1, p13, p23)
                    if status1 == dlstatus.STATUS_DOWNLOADINGDATA then
                      print(string.format('��������� %d �� %d.', p13, p23))
                    elseif status1 == dlstatus.STATUS_ENDDOWNLOADDATA then
                      print('�������� ���������� ���������.')
                      sampAddChatMessage((prefix..'���������� ���������!'), color)
                      goupdatestatus = true
                      lua_thread.create(function() wait(500) thisScript():reload() end)
                    end
                    if status1 == dlstatus.STATUSEX_ENDDOWNLOAD then
                      if goupdatestatus == nil then
                        sampAddChatMessage((prefix..'���������� ������ ��������. �������� ���������� ������..'), color)
                        update = false
                      end
                    end
                  end
                )
                end, prefix
              )
            else
              update = false
              print('v'..thisScript().version..': ���������� �� ���������.')
            end
          end
        else
          print('v'..thisScript().version..': �� ���� ��������� ����������. ��������� ��� ��������� �������������� �� '..url)
          update = false
        end
      end
    end
  )
  while update ~= false do wait(100) end
end
